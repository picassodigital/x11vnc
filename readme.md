I am developing a provisioning framework called Picasso.
Currently it only works on my LAN.

I have it working for server instances.

I can run "picasso create server", and a VM instance will be born.

I am now trying to create a generic desktop instance.

Currently I am working with Virtualbox. 
Virtualbox presents nice text and graphic interfaces to VMs,
so a fundamental question would be why I am developing my own text and graphic means to access VMs it provisions?
Because Virtualbox interfaces only run locally, while Picasso is intended to provide remote access.

In the end I should be able to provision an Openstack or AWS instance with terraform and enjoy the same text and graphic access to each instance.


The bottom line is that I desire to get the following going...


1) VNC

host> picasso create desktop  # this sets the environment variable IP1 which is the VM's IP
host> vncviewer $IP1:5900

the VM's linux desktop should appear in the local vnc viewer


2) X11-forwarding

host> picasso create desktop 
host> ssh -X -i <ssh key> picasso:$IP1
guest> startx

the remote linux desktop should appear within a native window on the user's system


3) wayland

if it is ready enough - I don't know


This readme is to accompany the 'bootstrap' file.

Bootstrap is a hybrid file - meaning that part of it executes on the host and part of it executes within the guest VM.
Picasso works in other ways, but this hybrid method has proven convenient because enverything is encapsulated within the one file.

The file makes extensive use of the bash heredoc mechanism. This is because I have my editor (Windows:notepad++) configured to display those sections in a color that pertains to their tag.

A bash example herdoc operates as follows...

[COMMAND] <<[-] 'DELIMITER'
  HERE-DOCUMENT
DELIMITER

An example herdoc from the included source is as follows...

:<<\_c
this is a multiline section of text
blah
blah
blah
_c

the COMMAND is ':'  # no-op
the DELIMITER is '_c'

that results in my embedded text being directed to a no-op command which effectively does nothing
that bashism has become my way of embedding various text within my script that gets executed

:<<\_c
comments
_c

:<<\_x
optional script
_x

:<<\_s
skipped script
_s

:<<\_j
junk
_j

all the above herdocs display in different color within my editior (Windows: notepad++) to highlight the different aspects.

I suggest you somehow overlook those sections by discarding them out of the source so they are not confusing you.


Picasso is designed to provision a vm as follows...

1) pre-processing - host configuration that must take place before guest provisioning
2) instantiates the VM
2.1) pre-provisiong - guest provisioning that must take place before our provisioning script
2.2) provisioning - our guest provisioning script is executed
2.3) post-provisioning - guest provisioing that must take place after our provisioning script
3) post-processing - host configuration that must take place after guest provisioning

within the bootstrap file there are different sections

guest provisioning consists of configuring the guest's environment and then running bash script

The content of PROVISIONER_ENV is passed through and sourced in the guest

cat > $PROVISIONER_ENV <<!
USERNAME=picasso
!

The content of INJECTION is passed through and executed in the guest

cat > $INJECTION <<\injection!  # strong - variables come from PROVISIONER_ENV
echo "Hello, $USERNAME!"
injection!

if this project was called 'test1'

picasso create test1

would produce this output...

Hello, picasso!

To simulate Picasso's provisioning within a standard Virtualbox instance all one has to do is cut and paste the content between the two sections above into the guest's bash terminal like so...

USERNAME=picasso
echo "Hello, $USERNAME!"


Do that with the content of those sections within the bootstrap file, and that should providee you with the same instance as that which Picasso would provision with the file.

Picasso is designed to take your bash code and provision it. In other words, you can develop without considering any framework constraints.

I don't know why I bothered to explain all that -- got off on a tangent I guess. At the very least you can gain insight into what I am working with.

I quickly edited bootstrap down into the file guest-script.sh which is the essence of the vnc code I have. I had all this working, but time passed and shit happened.

The fact is that I need help. If you prove effective then there is stuff to do. If you don't, then I underestand and have to move on.

Rolande Kendal
r.kendal@gmail.com

