
sudo apt-get update
sudo apt-get install -qqy x11vnc net-tools


sudo usermod -G video -a $USER  # display
sudo usermod -G input -a $USER  # keyboard & mouse

sudo usermod -G gdm -a $USER

VNC_GEOMETRY=${VNC_GEOMETRY:-1280x1024}
VNC_DEPTH=${VNC_DEPTH:-16}  # color bits
VNC_PORT=${VNC_PORT:-5900}

_install xvfb  # required for 'x11vnc -create'

#VNC_DISPLAY=${VNC_DISPLAY:-0}  # <hostname>:<display>.<monitor>
VNC_DISPLAY=${VNC_DISPLAY:-1}  # <hostname>:<display>.<monitor>
#VNC_GEOMETRY=1280x1024  # WxH
VNC_GEOMETRY=1680x1050
VNC_DEPTH=24  # color bits
VNC_PORT=5900
BGCOLOR="#AAAAAA"

if [[ -n "$VNC_PASSWORD" ]]; then
VNC_USE_PASSWORD=${VNC_USE_PASSWORD:-password}  # password/nopassword
else
VNC_USE_PASSWORD=${VNC_USE_PASSWORD:-nopassword}  # password/nopassword
fi

sudo sed -i 's|#WaylandEnable=false|WaylandEnable=false|' /etc/gdm3/custom.conf
sudo systemctl restart gdm.service
sleep 2

#uid=$(sudo loginctl | grep ' gdm ' | awk '{ print $2 }')  # get gdm greeter's UID
uid=$(sudo loginctl list-sessions --no-legend | grep ' gdm ' | awk '{ print $2 }')  # get gdm greeter's UID
MIT_MAGIC_COOKIE=/run/user/${uid}/gdm/Xauthority

sudo mkdir $HOME/.vnc
sudo chown $USER:users $HOME/.vnc

VNC_LOG_FILE=$HOME/.vnc/x11vnc.${VNC_DISPLAY}.log
VNC_CONF_PATH=$HOME/.vnc

# Fix the annoying .profile � ioctl � error message at login
sudo sed -i 's/mesg n || true/tty -s && mesg n || true/' $HOME/.profile


X11VNC_EXECSTART="$(which x11vnc) -rfbport ${VNC_PORT} -no6 -o ${VNC_LOG_FILE}"

X11VNC_EXECSTART+=" -forever"

case $VNC_USE_PASSWORD in
password) X11VNC_EXECSTART+=" -rfbauth ${VNC_CONF_PATH}/passwd";;
nopassword) X11VNC_EXECSTART+=" -nopw";;
esac

X11VNC_EXECSTART+=" -shared"

X11VNC_EXECSTART+=" -permitfiletransfer"

X11VNC_EXECSTART+=" -geometry ${VNC_GEOMETRY}x${VNC_DEPTH}"

X11VNC_EXECSTART+=" -xkb -noxrecord -noxfixes -noxdamage -nodpms -nocursor"

X11VNC_EXECSTART+=" -display :${VNC_DISPLAY}"

#allow="10.0.2.2"  # virtualbox's proxyforwarding
#MNIC_C=192.168.1
#allow+=",${MNIC_C}."

#X11VNC_EXECSTART+=" -allow $allow"

#X11VNC_EXECSTART+=" -auth /var/gdm/:0.Xauth"
#X11VNC_EXECSTART+=" -auth /var/lib/gdm/:0.Xauth"
X11VNC_EXECSTART+=" -auth $MIT_MAGIC_COOKIE"
#X11VNC_EXECSTART+=" -auth guess"
X11VNC_EXECSTART+=" -create"

X11VNC_EXECSTOP="/usr/bin/killall -u $USER /usr/bin/x11vnc"

sudo loginctl enable-linger $USER  # give the user permission to run services when they're not logged in

mkdir -p $VNC_CONF_PATH

case $VNC_USE_PASSWORD in
password)
x11vnc -storepasswd $VNC_PASS $VNC_CONF_PATH/passwd 2>&1
chmod 400 $VNC_CONF_PATH/passwd
;;
esac


cat > ~/.xinitrc <<\!
#!/bin/sh
if [ -d /etc/X11/xinit/xinitrc.d ]; then
  for f in /etc/X11/xinit/xinitrc.d/*; do
    [ -x "$f" ] && . "$f"
  done
  unset f
fi

[ -f ~/.Xresources ] && xrdb -merge -I$HOME ~/.Xresources
!
chmod u+x ~/.xinitrc

WM_START=/usr/bin/gnome-session

cat >> ~/.xinitrc <<!
xsetroot -solid '#CCCCCC' -cursor_name left_ptr
#echo ".xinitrc - RUNNING_UNDER_GDM: $RUNNING_UNDER_GDM, XDG_VTNR: \$XDG_VTNR"
exec $WM_START
!
#exec $WM_START --disable-wm-check

mkdir -p ~/.config/pulse

cat > ~/.vnc/xstartup <<!
#!/bin/bash
xrdb $HOME/.Xresources
!
chmod u+x ~/.vnc/xstartup


mkdir -p ~/.config/systemd/user

cat > ~/.config/systemd/user/Xvfb@USER.service <<!
[Unit]
Description=Xvfb
After=network.target syslog.target
Before=XvfbWM@USER.service x11vnc@USER.service
[Service]
Type=simple
#User=USER
Environment=DISPLAY=:${VNC_DISPLAY}
#ExecStart=/usr/bin/xinit -- /usr/bin/Xvfb :${VNC_DISPLAY} -screen 0 ${VNC_GEOMETRY}x${VNC_DEPTH} -nolisten tcp
ExecStart=/usr/bin/startx -- /usr/bin/Xvfb :${VNC_DISPLAY} -screen 0 ${VNC_GEOMETRY}x${VNC_DEPTH} -nolisten tcp
ExecStop=/usr/bin/killall -u $USER -w /usr/bin/Xvfb
ExecStopPost=/usr/bin/killall -u $USER /usr/bin/xinit
NoNewPrivileges=true
StandardOutput=journal
[Install]
WantedBy=default.target
!

cat > ~/.config/systemd/user/XvfbWM@USER.service <<!
[Unit]
Description=XvfbWM
Requires=Xvfb@USER.service
After=Xvfb@USER.service
Before=x11vnc@USER.service
[Service]
Type=simple
#User=USER
#EnvironmentFile=/etc/VNCServer/USER.cfg
Environment=DISPLAY=:${VNC_DISPLAY}
ExecStart=/usr/bin/openbox --startup "xsetroot -display :${VNC_DISPLAY} -solid ${BGCOLOR}"
#ExecStart=/user/bin/openbox --startup "xsetroot -display :${VNC_DISPLAY} -solid #AAAAAA"
[Install]
WantedBy=default.target
!

cat > ~/.config/systemd/user/x11vnc@USER.service <<!
[Unit]
Description=x11vnc
Requires=Xvfb@USER.service XvfbWM@USER.service
After=XvfbWM@USER.service
[Service]
Type=forking
#User=USER
Environment=DISPLAY=:${VNC_DISPLAY}
#ExecStartPre=/usr/bin/which x-window-manager
ExecStart=$X11VNC_EXECSTART
ExecStop=$X11VNC_EXECSTOP
NoNewPrivileges=true
StandardOutput=journal
[Install]
WantedBy=default.target
!

systemctl --user daemon-reload


systemctl --user start Xvfb@USER.service XvfbWM@USER.service x11vnc@USER.service
systemctl --user enable Xvfb@USER.service XvfbWM@USER.service x11vnc@USER.service 2>&1

